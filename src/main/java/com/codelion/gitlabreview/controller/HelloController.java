package com.codelion.gitlabreview.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hello")
public class HelloController {
    @GetMapping("/test")
    public static ResponseEntity test() {
        return ResponseEntity.status(HttpStatus.OK).body("hello likeLion? :-)");
    }
}

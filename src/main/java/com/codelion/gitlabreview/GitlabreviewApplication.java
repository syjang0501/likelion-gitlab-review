package com.codelion.gitlabreview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabreviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabreviewApplication.class, args);
    }

}
